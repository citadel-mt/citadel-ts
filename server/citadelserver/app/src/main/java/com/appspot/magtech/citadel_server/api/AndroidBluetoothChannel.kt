package com.appspot.magtech.citadel_server.api

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothServerSocket
import android.bluetooth.BluetoothSocket
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.os.Parcelable
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.appspot.magtech.citadel_server.sendLog
import com.appspot.magtech.citadelcore.ChannelDependentDevice
import com.appspot.magtech.citadelcore.ServerChannel
import com.appspot.magtech.citadelcore.utils.CitadelEvent
import kotlinx.coroutines.*
import kotlinx.coroutines.sync.Mutex
import kotlinx.coroutines.sync.withLock
import java.io.BufferedWriter
import java.io.IOException
import java.io.OutputStreamWriter
import java.util.*

class AndroidBluetoothChannel : ServerChannel {

    private var flagRunning = false

    private val NAME = "com.appspot.magtech.BluetoothService"
    private val SERVICE_UUID = UUID.fromString("00001805-0000-1000-8000-00805F9B34FB")

    private var currentId = 0
    private val sockets = mutableListOf<Pair<ChannelDependentDevice, BluetoothSocket>>()

    private inner class AcceptThread : Thread() {

        private val mmServerSocket: BluetoothServerSocket? by lazy(LazyThreadSafetyMode.NONE) {
            getBluetoothServerSocket()
        }

        private fun getBluetoothServerSocket(): BluetoothServerSocket? {
            val adapter = BluetoothAdapter.getDefaultAdapter()
            return try {
                val listener = adapter.javaClass.getMethod(
                    "listenUsingInsecureRfcommWithServiceRecord",
                    String::class.java,
                    UUID::class.java
                )
                listener.invoke(adapter, NAME, SERVICE_UUID) as? BluetoothServerSocket
            } catch (e: Exception) {
                null
            }
        }

        override fun run() {
            flagRunning = true
            while (flagRunning) {
                val socket: BluetoothSocket? = try {
                    mmServerSocket?.accept()
                } catch (e: IOException) {
                    null
                }
                socket?.also {
                    val device = ChannelDependentDevice(currentId, it.remoteDevice.name)
                    CoroutineScope(Dispatchers.IO).launch {
                        while (it.isConnected && flagRunning) {
                            var str = ""
                            var byte = 0
                            while (byte != -1 && byte != 255) {
                                byte = withContext(Dispatchers.IO) { it.inputStream.read() }
                                if (byte != -1 && byte != 255) {
                                    str += byte.toChar()
                                }
                            }
                            onReceiveMessageEvent.notifyAll(str)
                        }
                    }
                    sockets += device to it
                    onDeviceConnectedEvent.notifyAll(device)
                }
            }
        }
    }

    /* private val broadcastManager = LocalBroadcastManager.getInstance(context)

     private val broadcastReceiver = object : BroadcastReceiver() {

         override fun onReceive(context: Context?, intent: Intent?) {
             if (intent != null) {
                 when (intent.action) {
                     BluetoothService.DEVICE_CONNECT_ACTION -> {
                         val device = intent.extras?.get(BluetoothService.DEVICE_CONNECT_PARAM)
                                 as ChannelDependentDevice
                         onDeviceConnectedEvent.notifyAll(device)
                     }
                     BluetoothService.RECEIVE_ACTION -> {
                         val message = intent.getStringExtra(BluetoothService.RECEIVE_PARAM)
                         if (message != null) {
                             onReceiveMessageEvent.notifyAll(message)
                         }
                     }
                     BluetoothService.END_INIT_ACTION -> {

                     }
                     BluetoothService.END_SEND_ACTION -> {
                         sendMessageMutex.unlock()
                     }
                 }
             }
         }
     }*/

    override val onDeviceConnectedEvent = CitadelEvent<ChannelDependentDevice>()

    override val onReceiveMessageEvent = CitadelEvent<String>()

    override fun initializeAsync(): Deferred<Unit> {
        AcceptThread().start()
        return CoroutineScope(Dispatchers.IO).async { Unit }
    }

    override fun sendMessageAsync(message: String): Deferred<Unit> {
        return CoroutineScope(Dispatchers.IO).async {
            sockets.forEach {
                val writer = BufferedWriter(OutputStreamWriter(it.second.outputStream))
                writer.write(message)
                writer.write("\n")
                writer.flush()
            }
            Unit
        }
    }

    override fun terminate() {
        flagRunning = false
        sockets.forEach {
            it.second.close()
        }
    }
}