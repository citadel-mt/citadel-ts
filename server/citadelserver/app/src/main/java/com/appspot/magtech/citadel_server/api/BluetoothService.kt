package com.appspot.magtech.citadel_server.api

import android.app.*
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import com.appspot.magtech.citadel_server.sendLog
import com.example.citadel_server.R
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class BluetoothService : Service() {

    companion object {
        private const val ACTION_PREFIX = "com.appspot.magtech.citadel"

        const val START_COMMAND = "$ACTION_PREFIX.StartCommand"
        const val STOP_COMMAND = "$ACTION_PREFIX.StopCommand"

        /*const val INIT_ACTION = "$ACTION_PREFIX.InitAction"
        const val END_INIT_ACTION = "$ACTION_PREFIX.EndInitAction"

        const val TERMINATE_ACTION = "$ACTION_PREFIX.TerminateAction"

        const val SEND_ACTION = "$ACTION_PREFIX.SendAction"
        const val SEND_PARAM = "$ACTION_PREFIX.SendExtra"
        const val END_SEND_ACTION = "$ACTION_PREFIX.EndSendAction"*/

        const val DEVICE_CONNECT_ACTION = "$ACTION_PREFIX.DeviceConnectAction"
        const val DEVICE_CONNECT_PARAM = "$ACTION_PREFIX.DeviceConnectExtra"

        const val DEVICE_DISCONNECT_ACTION = "$ACTION_PREFIX.DeviceDisconnectAction"
        const val DEVICE_DISCONNECT_PARAM = "$ACTION_PREFIX.DeviceDisconnectExtra"

        /*const val RECEIVE_ACTION = "$ACTION_PREFIX.ReceiveAction"
        const val RECEIVE_PARAM = "$ACTION_PREFIX.ReceiveExtra"*/
    }

    private val channel = AndroidBluetoothChannel()
    private val api = CitadelNotifierApi(channel)

    init {
        api.onClientConnected += { client ->
            sendLog("Client connected: $client")
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent?.action == STOP_COMMAND) {
            CoroutineScope(Dispatchers.IO).launch {
                api.terminate()
                CoroutineScope(Dispatchers.Main).launch {
                    stopSelf()
                }
            }
        } else {
            CoroutineScope(Dispatchers.Main).launch {
                api.initialize()
                startForeground(FOREGROUND_ID, createNotification("Connected devices: 0"))
                sendLog("Initialized")
            }
        }
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    val FOREGROUND_ID = 777
    val CHANNEL_ID = "seneschal_android_channel_01"
    val CHANNEL_NAME = "Seneschal Notification Listener"

    private fun createNotification(text: String): Notification {
        val builder = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(CHANNEL_ID, CHANNEL_NAME)
            Notification.Builder(this, CHANNEL_ID)
        } else {
            Notification.Builder(this)
        }

        builder.setSmallIcon(R.drawable.icon)
            .setContentTitle("Bluetooth Service")
            .setContentText(text)
        return builder.build()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(channelId: String, channelName: String): String {
        val chan = NotificationChannel(
            channelId,
            channelName, NotificationManager.IMPORTANCE_DEFAULT
        )
        chan.lockscreenVisibility = Notification.VISIBILITY_PRIVATE
        val service = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        service.createNotificationChannel(chan)
        return channelId
    }
}