package com.appspot.magtech.citadel_server.api

import com.appspot.magtech.citadel_server.sendLog
import com.appspot.magtech.citadelcore.*
import com.appspot.magtech.citadelcore.utils.CitadelEvent
import kotlinx.coroutines.Deferred

interface NotificationServerPlugin : CitadelPlugin {

    @Sender
    fun sendNotificationAsync(notification: AndroidNotification): Deferred<SimpleResponse>
}

class CitadelNotifierApi(
    private val channel: ServerChannel
) : NotifierApi {

    private val dispatcher = MessageDispatcher(channel)

    private val notificationPlugin = citadelPluginFrom<NotificationServerPlugin>(
        id = "com.appspot.magtech.citadel.Notification",
        channel = channel,
        dispatcher = dispatcher
    )
    private val servicePlugin = citadelPluginFrom<ServiceServerPlugin>(
        id = "com.appspot.magtech.citadel.Service",
        channel = channel,
        dispatcher = dispatcher
    )

    init {
        channel.onDeviceConnectedEvent += {
            sendLog("Device connected: $it")
        }
        servicePlugin.clientConnectedEvent.set {
            onClientConnected.notifyAll(it)
            CitadelServer(type = "seneschal-android", name = "citadel-server")
        }
        servicePlugin.clientDisconnectedEvent.set {
            onClientDisconnected.notifyAll(it)
            SimpleResponse("OK")
        }
    }

    override suspend fun initialize() = channel.initializeAsync().await()

    override val onClientConnected = CitadelEvent<CitadelClient>()
    override val onClientDisconnected = CitadelEvent<CitadelClient>()

    override suspend fun sendNotification(notification: AndroidNotification) {
        notificationPlugin.sendNotificationAsync(notification).await()
    }

    override suspend fun terminate() {
        servicePlugin.sendTerminateAsync(EmptyRequest).await()
        channel.terminate()
    }
}