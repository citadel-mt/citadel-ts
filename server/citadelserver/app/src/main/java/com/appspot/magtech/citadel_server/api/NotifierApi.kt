package com.appspot.magtech.citadel_server.api

import com.appspot.magtech.citadelcore.CitadelClient
import com.appspot.magtech.citadelcore.utils.CitadelEvent

data class AndroidNotification(
    val packageId: String,
    val title: String,
    val body: String
)

interface NotifierApi {

    suspend fun initialize()
    suspend fun terminate()

    val onClientConnected: CitadelEvent<CitadelClient>

    val onClientDisconnected: CitadelEvent<CitadelClient>

    suspend fun sendNotification(notification: AndroidNotification)
}