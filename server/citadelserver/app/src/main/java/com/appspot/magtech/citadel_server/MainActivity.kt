package com.appspot.magtech.citadel_server

import android.app.Service
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.IBinder
import androidx.compose.Composable
import androidx.ui.core.Alignment
import androidx.ui.core.Text
import androidx.ui.core.setContent
import androidx.ui.layout.Align
import androidx.ui.layout.FlexColumn
import androidx.ui.material.Button
import androidx.ui.material.MaterialTheme
import androidx.ui.tooling.preview.Preview
import com.appspot.magtech.citadel_server.api.BluetoothService
import com.appspot.magtech.citadelcore.CitadelClient
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.channels.actor
import oolong.Update
import oolong.effect.none

data class MainModel(
    val isStarted: Boolean,
    val connectedClients: List<CitadelClient>
)

sealed class Msg {
    object StartServer : Msg()
    object StopServer : Msg()
}

val update: Update<MainModel, Msg> = { msg, model ->
    when (msg) {
        Msg.StartServer -> model.copy(isStarted = true) to none()
        Msg.StopServer -> model.copy(isStarted = false) to none()
    }
}

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            greeting(name = "World") {
                startService(Intent(this, BluetoothService::class.java))
            }
        }
    }
}

@Composable
fun greeting(name: String, onStart: () -> Unit) {
    MaterialTheme {
        Align(alignment = Alignment.Center) {
            Button(text = "Start", onClick = onStart)
        }
    }
}
