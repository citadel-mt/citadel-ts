// const bluetooth = require('node-bluetooth');
// const device = new bluetooth.DeviceINQ();

const server = new (require('bluetooth-serial-port')).BluetoothSerialPortServer();

const CHANNEL = 1;
const UUID = "00001805-0000-1000-8000-00805F9B34FB";

server.on('data', function (buffer) {
    console.log('Received data from client: ' + buffer);

    console.log('Sending data to the client');
    server.write(Buffer.from('...'), function (err, bytesWritten) {
        if (err) {
            console.log('Error!');
        } else {
            console.log('Send ' + bytesWritten + ' to the client!');
        }
    });
});

server.listen(function (clientAddress) {
    console.log('Client: ' + clientAddress + ' connected!');
}, function (error) {
    console.error("Something wrong happened!:" + error);
}, {uuid: UUID, channel: CHANNEL});

// const btSerial = new (require('bluetooth-serial-port')).BluetoothSerialPort();

/*
device
    .on('finished',  console.log.bind(console, 'finished'))
    .on('found', function found(address, name){
        console.log('Found: ' + address + ' with name ' + name);
    }).scan();

device.listPairedDevices(console.log);*/
