import {
    BluetoothAdapter,
    BluetoothDeviceSuccessCallback,
    BluetoothManager,
    BluetoothSocket, BluetoothSocketSuccessCallback, BluetoothUUID
} from "../../src/@types/tizen/Bluetooth";
import {Tizen} from "../../src/@types/tizen";
import {SuccessCallback} from "../../src/@types/tizen/Tizen";

class BluetoothSocketMock implements BluetoothSocket {

    onmessage: SuccessCallback = undefined;
    onclose: SuccessCallback = undefined;

    close() {
    }

    readData = jest.fn(/*() =>
        strToBytes(JSON.stringify(new ServerHelloMessage(new CitadelServer("Seneschal-tizen"))))*/
    );

    writeData = jest.fn((data: Array<number>) => {
        setTimeout(() => {
            this.onmessage();
        }, 100);
        return data.length - 1;
    });
}

export const testSocket = new BluetoothSocketMock();

export function strToBytes(str: string): Array<number> {
    const myBuffer = [];
    const buffer = Buffer.from(str, 'utf8');
    for (let i = 0; i < buffer.length; i++) {
        myBuffer.push(buffer[i]);
    }
    myBuffer.push(-1);
    return myBuffer;
}

export function bytesToStr(bytes: Array<number>): string {
    const arr = [...bytes];
    arr.splice(arr.length - 1, 1);
    return new TextDecoder().decode(new Uint8Array(arr));
}

function testConnect(uuid: BluetoothUUID, callback: BluetoothSocketSuccessCallback) {
    callback(testSocket);
}

export class BluetoothAdapterMock implements BluetoothAdapter {

    static devices = [
        {
            name: "Device 1",
            address: "Address 1",
            connectToServiceByUUID: testConnect
        },
        {
            name: "Device 2",
            address: "Address 2",
            connectToServiceByUUID: testConnect
        }
    ];

    discoverDevices(successCallback, errorCallback) {
        successCallback.onstarted();
        successCallback.ondevicefound(BluetoothAdapterMock.devices[0]);
        successCallback.ondevicefound(BluetoothAdapterMock.devices[1]);
        successCallback.onfinished(BluetoothAdapterMock.devices);
    }

    createBonding(address: string, successCallback: BluetoothDeviceSuccessCallback, errorCallback?: (WebAPIError) => void): void {
    }

    stopDiscovery(successCallback?: () => void, errorCallback?: (WebAPIError) => void): void {
    }
}

class BluetoothManagerMock implements BluetoothManager {

    getDefaultAdapter(): BluetoothAdapter {
        return new BluetoothAdapterMock();
    }
}

export class TizenMock implements Tizen {
    bluetooth = new BluetoothManagerMock();
}