module.exports = {
    transform: {'^.+\\.ts?$': 'ts-jest'},
    testRegex: '^.+\\.test\\.ts$',
    moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx', 'json', 'node'],
    globals: {
        TextDecoder: function () {
            return {
                decode: (arr) => {
                    let str = '';
                    for (let i = 0; i < arr.length; i++) {
                        str += String.fromCharCode(arr[i]);
                    }
                    return str;
                }
            };
        }
    }
};