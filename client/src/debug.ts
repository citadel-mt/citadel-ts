export default class Log {

    private static ip = '192.168.0.105';

    public static debug(message: string) {
        const xmlhttp = new XMLHttpRequest();
        xmlhttp.open('POST', `http://${this.ip}:3333?log=${message}`);
        xmlhttp.send();
    }
}