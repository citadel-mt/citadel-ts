import {SuccessCallback, ErrorCallback} from "./Tizen";

export interface BluetoothManager {
    getDefaultAdapter(): BluetoothAdapter
}

export interface BluetoothAdapter {

    discoverDevices(successCallback: BluetoothDiscoverDevicesSuccessCallback, errorCallback?: ErrorCallback): void

    stopDiscovery(successCallback?: SuccessCallback, errorCallback?: ErrorCallback): void

    createBonding(address: BluetoothAddress, successCallback: BluetoothDeviceSuccessCallback, errorCallback?: ErrorCallback): void
}

export interface BluetoothSocket {
    onmessage?: SuccessCallback
    onclose?: SuccessCallback

    writeData(data: Array<number>): number

    readData(): Array<number>

    /**
     * @throws WebApiException
     */
    close()
}

export interface BluetoothDevice {
    readonly name: string
    readonly address: BluetoothAddress

    connectToServiceByUUID(uuid: BluetoothUUID, successCallback: BluetoothSocketSuccessCallback, errorCallback?: ErrorCallback)
}

export type BluetoothUUID = string

export type BluetoothAddress = string

export interface BluetoothDeviceSuccessCallback {
    onsuccess(device: BluetoothDevice): void
}

export type BluetoothSocketSuccessCallback = (socket: BluetoothSocket) => void

export interface BluetoothDiscoverDevicesSuccessCallback {
    onstarted(): void

    ondevicefound(device: BluetoothDevice): void

    ondevicedisappeared(address: BluetoothAddress): void

    onfinished(foundDevices: Array<BluetoothDevice>): void
}