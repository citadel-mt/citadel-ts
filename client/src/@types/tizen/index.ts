import {BluetoothManager} from "./Bluetooth";

export interface Tizen {
    readonly bluetooth: BluetoothManager;
}