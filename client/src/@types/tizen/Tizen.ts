export type SuccessCallback = () => void;

export type ErrorCallback = (WebAPIError) => void;

interface WebAPIError {
    readonly code: number
    readonly name: string
    readonly message: string
}

export interface WebApiException {
    readonly code: number
    readonly name: string
    readonly message: string
}