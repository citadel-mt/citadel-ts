import { ChannelDependentDevice, ClientChannel, CitadelEvent } from 'citadel-core';
import { BluetoothAdapter, BluetoothDevice, BluetoothSocket } from "../@types/tizen/Bluetooth";

import { Tizen } from "../@types/tizen";
import Log from '../debug';

declare const tizen: Tizen;

export default class BluetoothClientChannel implements ClientChannel {

    private adapter: BluetoothAdapter = undefined;
    private socket: BluetoothSocket = undefined;

    private availableDevices: Map<number, BluetoothDevice> = new Map<number, BluetoothDevice>();
    private defaultUUID = "00001805-0000-1000-8000-00805F9B34FB";

    readonly onReceiveMessageEvent: CitadelEvent<string>;

    constructor() {
        this.adapter = tizen.bluetooth.getDefaultAdapter();
        this.onReceiveMessageEvent = new CitadelEvent<string>();
    }

    connectAsync(device: ChannelDependentDevice): Promise<void> {
        return new Promise((resolve: () => void, reject: (err: Error) => void) => {
            const bluetoothDevice = this.availableDevices.get(device.id);
            bluetoothDevice.connectToServiceByUUID(this.defaultUUID, (socket: BluetoothSocket) => {
                this.socket = socket;
                this.socket.onmessage = this.onMessage;
                resolve();
            }, (error) => {
                reject(error);
            });
        });
    }

    disconnectAsync(): Promise<void> {
        return new Promise(
            (resolve: () => void, reject: (err: Error) => void) => {
                try {
                    this.socket.close();
                    this.socket = undefined;
                    resolve();
                } catch (err) {
                    reject(err);
                }
            }
        );
    }

    private onMessage = () => {
        try {
            const arr = this.socket.readData();
            const str = this.bytesToStr(arr);
            this.onReceiveMessageEvent.notifyAll(str);
        } catch(err) {
            Log.debug(`Error: ${err}`);
        }
    }

    private strToBytes(str: string): Array<number> {
        const myBuffer = [];
        const buffer = Buffer.from(str, 'utf8');
        for (let i = 0; i < buffer.length; i++) {
            myBuffer.push(buffer[i]);
        }
        myBuffer.push(-1);
        return myBuffer;
    }

    private bytesToStr(bytes: Array<number>): string {
        const arr = [...bytes];
        arr.splice(arr.length - 1, 1);
        return new TextDecoder().decode(new Uint8Array(arr));
    }

    scanDevicesAsync(): Promise<Array<ChannelDependentDevice>> {
        return new Promise<Array<ChannelDependentDevice>>(
            (resolve: (value: Array<ChannelDependentDevice>) => void, reject: (err: Error) => void) => {
                const channel = this;
                this.adapter.discoverDevices({
                    onstarted() {
                    },
                    ondevicefound(device) {
                    },
                    onfinished(foundDevices) {
                        channel.availableDevices.clear();
                        const chdDevices = new Array<ChannelDependentDevice>();
                        foundDevices.forEach((device: BluetoothDevice, index: number) => {
                            const chdDevice = {
                                id: index,
                                name: device.name
                            };
                            chdDevices.push(chdDevice);
                            channel.availableDevices.set(index, device);
                        });
                        resolve(chdDevices);
                    },
                    ondevicedisappeared(address) {
                    }
                }, (error) => {
                    reject(error);
                }
                );
            });
    }

    sendMessageAsync(message: string): Promise<void> {
        if (this.socket != undefined) {
            return new Promise(
                (resolve: () => void, reject: (err: Error) => void) => {
                    try {
                        this.socket.writeData(this.strToBytes(message));
                        resolve();
                    } catch (err) {
                        reject(err);
                    }
                }
            );
        } else {
            return Promise.reject(new Error("Connection not established"));
        }
    }
}
