import {
    ClientChannel,
    ChannelDependentDevice,
    MessageDispatcher,
    ServiceClientPlugin,
    citadelPluginFrom,
    CitadelClient,
    CitadelServer
} from "citadel-core";
import Log from "../debug";

export default class BluetoothApi {

    private channel: ClientChannel;
    private servicePlugin: ServiceClientPlugin;
    private client: CitadelClient;

    constructor(channel: ClientChannel) {
        this.channel = channel;
        this.servicePlugin = citadelPluginFrom(
            "com.appspot.magtech.citadel.Service",
            this.channel,
            new MessageDispatcher(this.channel),
            new ServiceClientPlugin()
        );
        this.client = {
            type: "constable-tizen",
            name: "citadel-client"
        };
    }

    async scanDevices(): Promise<Array<ChannelDependentDevice>> {
        return this.channel.scanDevicesAsync();
    }

    async connect(device: ChannelDependentDevice): Promise<CitadelServer> {
        return new Promise(
            (resolve: (server: CitadelClient) => void, reject: (err: Error) => void) => {
                this.channel.connectAsync(device)
                    .then(() => {
                        this.servicePlugin.sendHelloAsync(this.client)
                            .then((server: CitadelServer) => {
                                resolve(server);
                            })
                            .catch(err => {
                                reject(err);
                            })
                    })
                    .catch(err => {
                        reject(err);
                    });
            }
        );
    }

    async disconnect(): Promise<void> {
        return new Promise(
            (resolve: () => void, reject: (err: Error) => void) => {
                this.servicePlugin.sendDisconnectAsync(this.client)
                    .then(() => {
                        resolve();
                    })
                    .catch(err => {
                        reject(err);
                    })
            }
        );
    }
}