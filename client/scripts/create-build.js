const dotenv = require('dotenv');
const { commandLine } = require('./utils');

const config = dotenv.config();

commandLine(`${process.env.TIZEN_HOME}/tizen build-web -- ./tizen`)
    .then((out) => {
        console.log(out);
    })
    .catch((err) => {
        console.error(err);
    });
