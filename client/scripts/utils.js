const {exec} = require('child_process');

function commandLine(args) {
    return new Promise((res, rej) => {
        exec(args, (err, stdout) => {
            if (err) {
                rej(err);
            } else {
                res(stdout);
            }
        })
    });
}

module.exports = {
    commandLine
};

