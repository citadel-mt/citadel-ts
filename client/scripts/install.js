const dotenv = require('dotenv');
const { commandLine } = require('./utils');

const config = dotenv.config();

commandLine(`${process.env.TIZEN_HOME}/tizen install -n ${process.env.APP_NAME}.wgt -t ${process.env.TARGET_NAME}  -- ./tizen/.buildResult/`)
    .then((out) => {
        console.log(out);
    })
    .catch((err) => {
        console.error(err);
    });
