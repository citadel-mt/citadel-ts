const dotenv = require('dotenv');
const { commandLine } = require('./utils');
const express = require('express');
const bodyParser = require('body-parser');

const config = dotenv.config();
const app = express();

commandLine(`${process.env.TIZEN_HOME}/tizen run -p ${process.env.PACKAGE_ID} -t ${process.env.TARGET_NAME}`)
    .then((out) => {
        console.log(out);
        app.listen('3333', () => {
            console.log("Debug session started");
        });
    })
    .catch((err) => {
        console.error(err);
    }); 
    
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: true,
  }),
);    
    
app.post('/', async (req, res) => {
    console.log(req.query.log);
    res.send("OK");
});
