const dotenv = require('dotenv');
const { commandLine } = require('./utils');

const config = dotenv.config();

commandLine(`${process.env.TIZEN_HOME}/tizen package -t wgt --sign ${process.env.SIGN_ALIAS} -- ./tizen/.buildResult/`)
    .then((out) => {
        console.log(out);
    })
    .catch((err) => {
        console.error(err);
    });
