const path = require('path');
const webpack = require('webpack');

const paths = {
    dist: path.resolve(__dirname, 'tizen/.buildResult')
};

module.exports = {
    resolve: {
		alias: {
			svelte: path.resolve('node_modules', 'svelte')
		},
		extensions: ['.mjs', '.js', '.ts', '.svelte'],
		mainFields: ['svelte', 'browser', 'module', 'main']
	},
    entry: {
        app: path.join(__dirname, 'src', 'index.js')
    },
    output: {
        path: paths.dist,
        filename: '[name].bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.(html|svelte)$/,
                exclude: /node_modules/,
                use: 'svelte-loader'
            },
            {
                test: /\.tsx?$/,
                loader: "ts-loader",
                options: { allowTsInNodeModules: true }
            }
        ]
    },
};
